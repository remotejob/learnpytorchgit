conda activate kaggle

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp datasets/dataset-metadata.json.almazseo data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp datasets/dataset-metadata.json.almazurov data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
cp datasets/dataset-metadata.json.kagkagdevprod data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp datasets/dataset-metadata.json.remotejob data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"



#GLOVE
export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp datasets/dataset-metadata.json.glove.remotejob dataglove/dataset-metadata.json
#kaggle datasets create -r zip -p dataglove/
kaggle datasets version -r zip -p data/ -m "Updated data 0"


#IMDB
export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp datasets/dataset-metadata.json.imdb.remotejob dataimdb/dataset-metadata.json
#kaggle datasets create -r zip -p dataimdb/
kaggle datasets version -r zip -p dataimdb/ -m "Updated data 0"


#MLBOT
export KAGGLE_CONFIG_DIR=/home/juno/kagglealesandermazurov
cp datasets/dataset-metadata.json.mlbot.alesandermazurov databot/dataset-metadata.json
#kaggle datasets create -r zip -p databot/
kaggle datasets version -r zip -p databot/ -m "Updated data 0"#MLBOT


#FASTBERT
export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp datasets/dataset-metadata.json.fastbert.remotejob fastbertdata/dataset-metadata.json
#kaggle datasets create -r zip -p fastbertdata/
kaggle datasets version -r zip -p fastbertdata/ -m "Updated data 0"

#simpletransfomers
export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp datasets/dataset-metadata.json.simpletransformers.almazurov fastbertdata/dataset-metadata.json
#kaggle datasets create -r zip -p fastbertdata/
kaggle datasets version -r zip -p fastbertdata/ -m "Updated data 0"


#simpletransbot
export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp datasets/dataset-metadata.json.simpletransbot.almazseo simpletransdata/dataset-metadata.json
#kaggle datasets create -r zip -p simpletransdata/
kaggle datasets version -r zip -p simpletransdata/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp datasets/dataset-metadata.json.simpletransbot.almazurov simpletransdata/dataset-metadata.json
#kaggle datasets create -r zip -p simpletransdata/
kaggle datasets version -r zip -p simpletransdata/ -m "Updated data 0"


#agnews
export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp datasets/dataset-metadata.json.agnewsdata.almazseo agnewsdata/dataset-metadata.json
#kaggle datasets create -r zip -p agnewsdata/
kaggle datasets version -r zip -p agnewsdata/ -m "Updated data 0"


#bertfinnish
export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp datasets/dataset-metadata.json.bertfinnish.almazurov bertfinnishdata/dataset-metadata.json
#kaggle datasets create -r zip -p bertfinnishdata/
kaggle datasets version -r zip -p bertfinnishdata/ -m "Updated data 0"