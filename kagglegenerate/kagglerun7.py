import os
# os.system('pip install -U wandb')

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
wandb.init(project="transformers")

os.system('pip install -U sentence-transformers')
from sentence_transformers import SentenceTransformer
import numpy as np


import torch
from torch.utils.data import Dataset,DataLoader,random_split
from torch import nn,optim
from torch.autograd import Variable
import torch.nn.functional as F


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

torch.manual_seed(1)


class FinBotDataset(Dataset):
    def __init__(self,data_root):
        self.samples = []
        # x_tn, y_tn = np.genfromtxt(data_root,
        #                    delimiter='\t', usecols=(1, 2), dtype=str, comments=None, unpack=True,max_rows=2000)
        x_tn, y_tn = np.genfromtxt(data_root,
                           delimiter='\t', usecols=(0, 1), dtype=str, comments=None, unpack=True,max_rows=309)                           
        modeltr = SentenceTransformer('bert-base-nli-mean-tokens')
        sentences = []

        for i in x_tn:
            sentences.append(i)

        x_tn_bert = modeltr.encode(sentences)
        for i in range(0,len(y_tn)):
            x,y =  x_tn_bert[i],np.array(float(y_tn[i]))
            self.samples.append((x,y))  

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        return self.samples[idx]
   

# dataset = FinBotDataset('../input/learnpytorchdata/sentences.tsv')
dataset = FinBotDataset('../input/learnpytorchdata/ask.tsv')

trainset, valset = random_split(dataset, [300, 9])

dataloader = DataLoader(dataset, batch_size=1, shuffle=True, num_workers=1)
valdataloader = DataLoader(valset, batch_size=1, shuffle=True, num_workers=1)


# model = nn.Sequential(nn.Linear(768, 128),
#                       nn.ReLU(),
#                       nn.Linear(128, 64),
#                       nn.ReLU(),
#                       nn.Linear(64, 6),
#                       nn.LogSoftmax(dim=1))

class BoWClassifier(nn.Module):  # inheriting from nn.Module!

    def __init__(self,num_labels, vocab_size):
        # calls the init function of nn.Module.  Dont get confused by syntax,
        # just always do it in an nn.Module
        super(BoWClassifier, self).__init__()

        # Define the parameters that you will need.  In this case, we need A and b,
        # the parameters of the affine mapping.
        # Torch defines nn.Linear(), which provides the affine map.
        # Make sure you understand why the input dimension is vocab_size
        # and the output is num_labels!
        self.linear = nn.Linear(vocab_size, num_labels)

        # NOTE! The non-linearity log softmax does not have parameters! So we don't need
        # to worry about that here

    def forward(self, x):
        # Pass the input through the linear layer,
        # then pass that through log_softmax.
        # Many non-linearities and other functions are in torch.nn.functional
        return F.log_softmax(self.linear(x), dim=1)                      

model =  BoWClassifier(2,768)


model = model.to(device)


criterion = nn.NLLLoss()
# criterion = nn.MSELoss()
# Optimizers require the parameters to optimize and a learning rate
optimizer = optim.SGD(model.parameters(), lr=0.05)


epochs = 20
for e in range(epochs):
    running_loss = 0
    for x,y in dataloader:
        optimizer.zero_grad()
        output = model(x.to(device))
        loss = criterion(output, y.to(device).long())
        wandb.log({"Loss": loss})
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
    else:
        print(f"Training loss: {running_loss/len(dataloader)}")



with torch.no_grad():
    for  instance, label  in valdataloader:
        # instance, label =  dataset[i]
        log_probs = model(instance.to(device))
        print(label,log_probs,log_probs.max(),log_probs.mean())
        print('---------------')



# print(next(model.parameters())[:, word_to_ix["creo"]])
# with torch.no_grad():

#     model.cpu()

#     for i in range(0,10):
#             # print(x_tn[i],y_tn[i])
#         x,y = dataset[i]
#         prval = model(torch.from_numpy(x))
#         # print(y[i],round(prval.item()),prval.item()) 
#         # print(y,prval)
#         print(y,prval.max(),prval.mean(),prval)        