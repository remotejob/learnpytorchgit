# https://medium.com/@sangramsing/language-modeling-in-torchtext-a9810954ab0c

# import os
# os.system('pip install -U wandb')

# os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
# import wandb
# wandb.init(project="transformers")

import tqdm 
# from tgdm import tgdm
from torchtext.datasets import WikiText2
import torchtext
from torchtext import data
import spacy
import torch.nn as nn
import torch.nn.functional as F
import math
import torch.optim as optim
from torch.autograd import Variable as V

from spacy.symbols import ORTH
my_tok = spacy.load('en')


def spacy_tok(x):
    return [tok.text for tok in my_tok.tokenizer(x)]


TEXT = data.Field(lower=True, tokenize=spacy_tok)

train, valid, test = WikiText2.splits(TEXT)

print(len(train))

TEXT.build_vocab(train, vectors="glove.6B.200d")

train_iter, valid_iter, test_iter = data.BPTTIterator.splits(
    (train, valid, test),
    batch_size=32,
    bptt_len=30,  # this is where we specify the sequence length
    device=0,
    repeat=False)



b = next(iter(train_iter))
print('-------------')
# print(len(WikiText2.))
print('-------------')
print(vars(b).keys())
print(b.text[:5, :3])
print(b.target[:5, :3])
print('---------------')


class RNNModel(nn.Module):
    """Container module with an encoder, a recurrent module, and a decoder."""

    def __init__(self, ntoken, ninp, nhid, nlayers, bsz, dropout=0.5, tie_weights=True):
        super(RNNModel, self).__init__()
        self.nhid, self.nlayers, self.bsz = nhid, nlayers, bsz
        self.drop = nn.Dropout(dropout)
        self.encoder = nn.Embedding(ntoken, ninp)
        self.rnn = nn.LSTM(ninp, nhid, nlayers, dropout=dropout)
        self.decoder = nn.Linear(nhid, ntoken)

        self.init_weights()
        self.hidden = self.init_hidden(bsz)

    def init_weights(self):
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.fill_(0)
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, input):
        emb = self.drop(self.encoder(input))
        output, self.hidden = self.rnn(emb, self.hidden)
        output = self.drop(output)
        decoded = self.decoder(output.view(
            output.size(0)*output.size(1), output.size(2)))
        # decoded = decoded.view(-1, self.ntoken)
        return decoded.view(output.size(0), output.size(1), decoded.size(1))

    def init_hidden(self, bsz):
        weight = next(self.parameters()).data
        return (V(weight.new(self.nlayers, bsz, self.nhid).zero_().cuda()),
                V(weight.new(self.nlayers, bsz, self.nhid).zero_()).cuda())

    def reset_history(self):
        self.hidden = tuple(V(v.data) for v in self.hidden)


weight_matrix = TEXT.vocab.vectors
BATCH_SIZE = 28870
n_tokens = 30

model = RNNModel(weight_matrix.size(0), weight_matrix.size(1),200, 1, BATCH_SIZE)

model.encoder.weight.data.copy_(weight_matrix)
model.cuda()

criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=1e-3, betas=(0.7, 0.99))

from tqdm import tqdm

def train_epoch(epoch):
    epoch_lost = 0
    for batch in tqdm(train_iter):
        model.reset_history()
    optimizer.zero_grad()
    text, targets = batch.text.cuda(), batch.target.cuda()
    prediction = model(text)
    # loss = criterion(prediction.view(-1, n_tokens), targets.view(-1))
    loss = criterion(prediction.view(-1,32), targets.view(-1))
    # wandb.log({"Loss": loss})
    loss.backwards()
    optimizer.step()

    epoch_lost += loss.data[0]*prediction.size(0) * prediction.size(1)
    epoch_lost /= len(train.examples[0].text)
    # val_loss = 0
    # model.eval()
    # for batch in valid_iter:
    #     model.reset_history()
    #     text, targets = batch.text, batch.target
    #     prediction = model(text)
    #     loss = criterion(prediction.view(-1, 23096000), targets.view(-1))
    #     val_loss += loss.data[0]*text.size(0)
    # val_loss /= len(valid.examples[0].text)
    print('Epoch: {},Train Loss {:.4f}'.format(
        epoch, epoch_lost))


# train_epoch(5)

for e in range(3):
    train_epoch(e)