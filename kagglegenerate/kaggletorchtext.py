import pandas as pd
import numpy as np
import torch
import time
import random
import os
from torchtext import data
from torchtext.vocab import Vectors
from torch.nn import init
from tqdm import tqdm
from torchtext.data import Iterator, BucketIterator
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchtext.vocab import GloVe
from torchtext.vocab import FastText

os.system('pip install -U wandb')

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
wandb.init(project="transformers")


device = 'cuda' if torch.cuda.is_available() else 'cpu'


def tokenize(x): return x.split()


# TEXT = data.Field(sequential=True, tokenize=tokenize, lower=True, fix_length=200)
TEXT = data.Field(sequential=True)
LABEL = data.Field(sequential=False, use_vocab=False)
# train_path = '/kaggle/input/learnpytorchdata/toxicsmall/train_one_label.csv'
# valid_path = "/kaggle/input/learnpytorchdata/toxicsmall/valid_one_label.csv"
# test_path = "/kaggle/input/learnpytorchdata/toxicsmall/test.csv"

# train_path = '/kaggle/input/learnpytorchdata/toxicsmall/train_preprocessed.csv'
# valid_path = "/kaggle/input/learnpytorchdata/toxicsmall/valid_preprocessed.csv"
# test_path = "/kaggle/input/learnpytorchdata/toxicsmall/test.csv"

train_path = '/kaggle/input/learnpytorchdata/toxic/train.csv'
valid_path = "/kaggle/input/learnpytorchdata/toxic/valid.csv"
test_path = "/kaggle/input/learnpytorchdata/toxic/test.csv"


class MyDataset(data.Dataset):

    def __init__(self, path, text_field, label_field, test=False, aug=False, **kwargs):
        fields = [("id", None), ("comment_text", text_field),
                  ("toxic", label_field)]

        examples = []
        if test:
            csv_data = pd.read_csv(path, error_bad_lines=False, usecols=[
                                   "comment_text"], header=0)
            # csv_data=pd.isna(csv_data['comment_text'])
            # csv_data= csv_data[csv_data['comment_text'].notnull() & csv_data['comment_text']!=u'']
            # csv_data[csv_data['comment_text'].apply(lambda x: x !='')]
            # csv_data[csv_data['comment_text'].apply(lambda x: print(x))]
        else:
            # csv_data0 = pd.read_csv(path,error_bad_lines=False,usecols=["comment_text","toxic"],skiprows=[i for i in range(1,100)])
            csv_data0 = pd.read_csv(path, error_bad_lines=False, usecols=[
                                    "comment_text", "toxic"], header=0)
            # csv_data['comment_text'].apply(lambda x: self.pandafilter(x))
            # csv_data= csv_data[csv_data['comment_text'].notnull() & csv_data['comment_text'] !='' ]
            # csv_data= csv_data[csv_data['comment_text'].str.contains('history', na=False)]
            csv_data1 = csv_data0[csv_data0.apply(
                self.pandafilter, axis=1, reduce=True)]
            csv_data = csv_data1[csv_data1.apply(
                self.pandafilternum, axis=1, reduce=True)]    
            # csv_data[csv_data['comment_text'].apply(lambda x: self.pandafilter(x))]
            # csv_data[csv_data['comment_text'].apply(lambda x: x =='')]
            # csv_data=pd.isna(csv_data['comment_text'])
            # csv_data = csv_data['comment_text'].apply(lambda x: len(x)>5)
        print('read data from {}'.format(path))

        if test:
            for text in tqdm(csv_data['comment_text']):
                examples.append(data.Example.fromlist(
                    [None, text, None], fields))
        else:
            for text, label in tqdm(zip(csv_data['comment_text'], csv_data['toxic'])):
                if aug:
                    # do augmentation
                    rate = random.random()
                    if rate > 0.5:
                        text = self.dropout(text)
                    else:
                        text = self.shuffle(text)
                # Example: Defines a single training or test example.Stores each column of the example as an attribute.
                examples.append(data.Example.fromlist(
                    [None, text, label], fields))
        # super(MyDataset, self).__init__(examples, fields, **kwargs)
        super(MyDataset, self).__init__(examples, fields)

    def pandafilter(self, row):
        # return isinstance(row['comment_text'], str)
        if isinstance(row['comment_text'], str):
            if row['comment_text'] != '':
                return True
            else:
                return False
        else:
            # print("Not string", row)
            return False

    def pandafilternum(self, row):
        # print(type(row['toxic']),row['toxic'])
        if isinstance(row['toxic'], str):
            if len(row['toxic']) == 1:
                if (row['toxic']== '0') or (row['toxic'] == '1'):
                    return True
                else:
                    return False
            else:
                return False        
        else:
            # print("Not num", row)
            return False

    def shuffle(self, text):
        text = np.random.permutation(text.strip().split())
        return ' '.join(text)

    def dropout(self, text, p=0.5):
        # random delete some text
        text = text.strip().split()
        len_ = len(text)
        indexs = np.random.choice(len_, int(len_ * p))
        for i in indexs:
            text[i] = ''
        return ' '.join(text)


class LSTM(nn.Module):

    def __init__(self, weight_matrix):
        super(LSTM, self).__init__()
        # embedding之后的shape: torch.Size([200, 8, 300])
        self.word_embeddings = nn.Embedding(len(TEXT.vocab), 300)
        self.word_embeddings.weight.data.copy_(weight_matrix)
        # embedding.weight.data.copy_(weight_matrix)
        self.lstm = nn.LSTM(input_size=300, hidden_size=128,
                            num_layers=1)  # torch.Size([200, 8, 128])
        self.decoder = nn.Linear(128, 2)

    def forward(self, sentence):
        embeds = self.word_embeddings(sentence.to(device))
        lstm_out = self.lstm(embeds)[0]  # lstm_out:200x8x128
        final = lstm_out[-1]  # 8*128
        y = self.decoder(final)  # 8*2
        return y


def data_iter(train_path, valid_path, test_path, TEXT, LABEL):
    train = MyDataset(train_path, text_field=TEXT,
                      label_field=LABEL, test=False, aug=None)
    valid = MyDataset(valid_path, text_field=TEXT,
                      label_field=LABEL, test=False, aug=None)
    test = MyDataset(test_path, text_field=TEXT,
                     label_field=None, test=True, aug=1)

    # cache = '.vector_cache'
    # if not os.path.exists(cache):
    #     os.mkdir(cache)

    if not os.path.exists('.vector_cache'):
        os.mkdir('.vector_cache')
    vectors = Vectors(
        # name='/kaggle/input/learnpytorchdata/glove/glove.6B.300d.txt')
        name='/kaggle/input/glove840b300dtxt/glove.840B.300d.txt')

    # TEXT.build_vocab(train,vectors=GloVe(name='6B', dim=300))
    # TEXT.build_vocab(train,vectors=FastText(language='en'))
    TEXT.build_vocab(train, vectors=vectors)
    # print(TEXT.vocab.stoi('history'))

    # TEXT.build_vocab(train)
    weight_matrix = TEXT.vocab.vectors
    # weight_matrix = None
    # 若只针对训练集构造迭代器
    # train_iter = data.BucketIterator(dataset=train, batch_size=8, shuffle=True, sort_within_batch=False, repeat=False)
    train_iter, val_iter = BucketIterator.splits(
        (train, valid),
        # batch_sizes=(8, 8),
        # batch_sizes=(16, 16),
        # batch_sizes=(64, 64),
        # batch_sizes=(128, 128),
        # batch_sizes=(256, 256),
        batch_sizes=(512, 512),
        device=0,
        # the BucketIterator needs to be told what function it should use to group the data.
        sort_key=lambda x: len(x.comment_text),
        sort_within_batch=False,
        repeat=False
    )
    test_iter = Iterator(test, batch_size=8, device=0,
                         sort=False, sort_within_batch=False, repeat=False)
    return train_iter, val_iter, test_iter, weight_matrix


def main():
    train_iter, val_iter, test_iter, weight_matrix = data_iter(
        train_path, valid_path, test_path, TEXT, LABEL)

    print('train_iter len->', len(train_iter))
    print('val_iter len->', len(val_iter))

    model = LSTM(weight_matrix)
    model.to(device)
    wandb.watch(model)
    # model.train()
    optimizer = optim.Adam(
        filter(lambda p: p.requires_grad, model.parameters()), lr=0.01)
    loss_funtion = F.cross_entropy

    for epoch in range(1, 50+1):
        model.train()
        for idx, batch in enumerate(train_iter):
            optimizer.zero_grad()
            predicted = model(batch.comment_text)

            loss = loss_funtion(predicted, batch.toxic.to(device))
            wandb.log({"train_loss": loss})
            loss.backward()
            optimizer.step()
            print('idx:', idx, 'loss:', loss)

        model.eval()
        with torch.no_grad():
            for epoch, batch in enumerate(val_iter):
                predicted = model(batch.comment_text.to(device))
                val_loss = loss_funtion(predicted, batch.toxic.to(device))
                print('val_idx:', epoch, 'val_loss:', loss)

    model.eval()
    print('---------------------------------')
    tokenized = tokenize("Go fuck yourself, cumshitter.")
    print(tokenized)
    indexed = [TEXT.vocab.stoi[t] for t in tokenized]
    print(indexed)
    length = [len(indexed)]
    print(length)
    tensor = torch.LongTensor(indexed).to(device)              #convert to tensor
    tensor = tensor.unsqueeze(1).T                             #reshape in form of batch,no. of words
    length_tensor = torch.LongTensor(length)                   #convert to tensor
    preds = model(tensor)
    max_preds = preds.argmax(dim = 1)
    print("mac_pred",max_preds)
    # prediction1 = model(length_tensor)
    # print("pred1",prediction1.mean())
    # print("pred",prediction.item())
    print('---------------------------------')

    print('---------------------------------')
    tokenized = tokenize("Jayjg, stop the Spin (public relations) tactics.")
    print(tokenized)
    indexed = [TEXT.vocab.stoi[t] for t in tokenized]
    print(indexed)
    length = [len(indexed)]
    print(length)
    tensor = torch.LongTensor(indexed).to(device)              #convert to tensor
    tensor = tensor.unsqueeze(1).T                             #reshape in form of batch,no. of words
    length_tensor = torch.LongTensor(length)                   #convert to tensor
    preds = model(tensor)
    max_preds = preds.argmax(dim = 1)
    print("mac_pred",max_preds)
 
    print('---------------------------------')


    print('---------------------------------')
    tokenized = tokenize("Hello world")
    print(tokenized)
    indexed = [TEXT.vocab.stoi[t] for t in tokenized]
    print(indexed)
    length = [len(indexed)]
    print(length)
    tensor = torch.LongTensor(indexed).to(device)              #convert to tensor
    tensor = tensor.unsqueeze(1).T                             #reshape in form of batch,no. of words
    length_tensor = torch.LongTensor(length)                   #convert to tensor
    preds = model(tensor)
    max_preds = preds.argmax(dim = 1)
    print("mac_pred",max_preds)
 
    print('---------------------------------')
   


    # for idx, batch in enumerate(train_iter):
    #     print(idx,batch.comment_text)


# embedding = nn.Embedding(2000, 300)
# # Specify the initial weight of the embedded matrix
# weight_matrix = TEXT.vocab.vectors
# embedding.weight.data.copy_(weight_matrix )
if __name__ == '__main__':
    main()
