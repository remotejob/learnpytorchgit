import os
import sys
# https://app.wandb.ai/cayush/pytorchlightning/reports/Use-Pytorch-Lightning-with-Weights-%26-Biases--Vmlldzo2NjQ1Mw

os.system('pip install pytorch_lightning')
os.system('pip install --upgrade wandb')
os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')

sweep_config = {
    'method': 'random',  # grid, random
    'metric': {
      'name': 'accuracy',
      'goal': 'maximize'
    },
    'parameters': {

        'learning_rate': {
            'values': [0.1, 0.01, 0.001]
        },
        'optimizer': {
            'values': ['adam', 'sgd']
        }
    }

}

import torch
from torch import nn
from torch.utils.data import DataLoader, random_split
from torch.nn import functional as F
from torchvision.datasets import MNIST
from torchvision import datasets, transforms
import wandb
import pytorch_lightning as pl
from pytorch_lightning.logging.wandb import WandbLogger
config_defaults = {
        'learning_rate': 0.1,
        'optimizer': 'adam',
}

class LightningMNISTClassifier(pl.LightningModule):

  def __init__(self):
    super(LightningMNISTClassifier, self).__init__()

    # mnist images are (1, 28, 28) (channels, width, height) 
    self.layer_1 = torch.nn.Linear(28 * 28, 128)
    self.layer_2 = torch.nn.Linear(128, 256)
    self.layer_3 = torch.nn.Linear(256, 10)

  def forward(self, x):
      batch_size, channels, width, height = x.size()

      # (b, 1, 28, 28) -> (b, 1*28*28)
      x = x.view(batch_size, -1)

      # layer 1
      x = self.layer_1(x)
      x = torch.relu(x)

      # layer 2
      x = self.layer_2(x)
      x = torch.relu(x)

      # layer 3
      x = self.layer_3(x)

      # probability distribution over labels
      x = torch.log_softmax(x, dim=1)

      return x

  def cross_entropy_loss(self, logits, labels):
    return F.nll_loss(logits, labels)
    
  def training_step(self, train_batch, batch_idx):
      x, y = train_batch
      logits = self.forward(x)   # we already defined forward and loss in the lightning module. We'll show the full code next
      loss = self.cross_entropy_loss(logits, y)

      logs = {'train_loss': loss}
      return {'loss': loss, 'log': logs}

  def validation_step(self, val_batch, batch_idx):
      x, y = val_batch
      logits = self.forward(x)
      loss = self.cross_entropy_loss(logits, y)
      return {'val_loss': loss}

  def validation_end(self, outputs):
      # outputs is an array with what you returned in validation_step for each batch
      # outputs = [{'loss': batch_0_loss}, {'loss': batch_1_loss}, ..., {'loss': batch_n_loss}]
      
      avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
      tensorboard_logs = {'val_loss': avg_loss}
      return {'avg_val_loss': avg_loss, 'log': tensorboard_logs}

  def prepare_data(self):
    # prepare transforms standard to MNIST
    MNIST(os.getcwd(), train=True, download=True)
    MNIST(os.getcwd(), train=False, download=True)

  def train_dataloader(self):
    transform=transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))])
    mnist_train = MNIST(os.getcwd(), train=True, download=False, 
                        transform=transform)
    self.mnist_train, self.mnist_val = random_split(mnist_train, [55000, 5000])

    mnist_train = DataLoader(self.mnist_train, batch_size=32,num_workers=2)
    return mnist_train
  def val_dataloader(self):
    mnist_val = DataLoader(self.mnist_val, batch_size=32,num_workers=2)
    return mnist_val


#   def test_dataloader(self):
#     #Load test data
#     mnist_test = DataLoader(mnist_test, batch_size=32)
#     return mnist_test

#   def test_dataloader(self):
#     transform=transforms.Compose([transforms.ToTensor(), 
#                                   transforms.Normalize((0.1307,), (0.3081,))])
#     mnist_test = MNIST(os.getcwd(), train=False, download=False, 
#                        transform=transform)
#     mnist_test = DataLoader(mnist_test, batch_size=32)
#     return mnist_test

  def configure_optimizers(self):
    # the lightningModule HAS the parameters (remember that we had the __init__ and forward method but we're just not showing it here)

    optimizer = torch.optim.Adam(self.parameters(),0.001)
    #optimizer =  torch.optim.SGD(self.parameters(),lr=0.01)
    return optimizer


# os.system('export CUDA_HOME=/usr/local/cuda-10.1')
# os.system('git clone https://github.com/NVIDIA/apex')
# os.system('pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./apex')

from pytorch_lightning.loggers import WandbLogger

wandb_logger = WandbLogger(name='TestRun-16-bit-adam-0.001',project='transformers')


model = LightningMNISTClassifier()
model.prepare_data()
model.train_dataloader()
#Change the GPU number to the number of gpus you wish to use
# trainer = pl.Trainer(max_epochs = 100,logger= wandb_logger, gpus=1, distributed_backend='dp',early_stop_callback=True,precision=32)
trainer = pl.Trainer(max_epochs = 100,logger= wandb_logger,gpus=1,distributed_backend='dp',early_stop_callback=True)

def train():
  trainer.fit(model)

train()

trainer.save_checkpoint('EarlyStoppingADam-32-100.pth')
wandb.save('EarlyStoppingADam-32-100.pth')
