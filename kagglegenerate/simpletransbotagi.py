import os
import csv


# os.system('/opt/conda/bin/python -m pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ../input/apexpytorch') 

os.system('pip install --upgrade transformers')
os.system('pip install simpletransformers')


import torch
import torch.nn as nn
from simpletransformers.classification import ClassificationModel
import numpy as np
import pandas as pd

server_args={
    'fp16': False,
    # 'num_train_epochs': 350,
    # 'save_model_every_epoch': False,
    # 'save_eval_checkpoints': False,
    # 'save_steps':2000,
    # 'overwrite_output_dir': True,
    # 'train_batch_size': 96, #8 #48 #
    # 'eval_batch_size': 72,
    # 'reprocess_input_data': True,
    'do_lower_case': True,
    # 'logging_steps': 100,
    # 'evaluate_during_training':True
}

datapath ='/kaggle/input/simpletransbotdata/'
# eval_df = 

# eval_df = pd.read_csv(datapath+'val.csv', error_bad_lines=False,skiprows=1,header=None)
# eval_df.dropna(inplace = True)

# eval_df.columns = ['text', 'labels']

# for txt in eval_df.text:
#     print(txt)





model = ClassificationModel('bert', '/kaggle/input/simpletransbotdata/best_model', args=server_args,use_cuda=True,num_labels=65)

# 'en tiedä' 18 'aloitan runkkaamisen' 2 olet nätti nainen 26 


# predictions, raw_outputs = model.predict(['en tiedä','aloitan runkkaamisen','olet nätti nainen','nykyään puhelimella'])

# print(predictions)
from scipy.special import softmax

# eval_df.apply(lambda x: print(type(x)))

notgood = 0
good =0
minscore = []
with open(datapath+'val.csv',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    # next(reader, None)
    for row in reader:
        artext = [row['ask']]
        labels = row['intent']
        print('Start------------')
        print(artext,'--',labels)

        predictions, raw_outputs = model.predict(artext)
        pred = predictions[0]
        raw = raw_outputs[0]
        # print(pred,raw)
        prob = np.argmax(raw)
        softmaxlabel = softmax(raw)
        score =softmaxlabel[pred]
        # print("score",type(score.item()),"minscore",type(minscore))
 
        if pred.item() != int(labels):
            print("Not good",pred,score)
            notgood = notgood+1
        else:
            # print("Good",pred,score)
            minscore.append(score.item())
            good = good +1

        print('End------------')

numscore =  np.array(minscore, dtype=np.float32)



print("scoreMIN",numscore.min(),"scoreMAX",numscore.max(),"scoreMIAN",numscore.mean())

print("not vs good",notgood/good)



