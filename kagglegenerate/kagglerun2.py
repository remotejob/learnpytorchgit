import os
os.system('pip install -U sentence-transformers')
from sentence_transformers import SentenceTransformer

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torch.autograd import Variable
import numpy as np
import random
from torch.utils.data import DataLoader, Dataset

input_size = 768 #6
hidden_size = 768 #6
num_classes = 2
num_epochs = 100
batch_size = 1
learning_rate = 0.01
numrec = 500

x_tn, y_tn = np.genfromtxt('../input/learnpytorchdata/sentences.tsv',
                           delimiter='\t', usecols=(1, 2), dtype=str, comments=None, unpack=True)

modeltr = SentenceTransformer('bert-base-nli-mean-tokens')

sentences = []

for i in range(0, numrec):
    sentences.append(x_tn[i])

sentence_embeddings = modeltr.encode(sentences)

training_data = sentence_embeddings

# y_train = torch.from_numpy(y_tn.astype(np.float))
# print(y_train.shape)
# y_train.resize_(10000, 1)
# y_tn = y_train
y = []
for i in range(0, numrec):
    y.append(int(y_tn[i]))

training_label = torch.LongTensor(y)

class MyDataset(Dataset):
    def __init__(self, datas, labels):
        self.datas = datas
        self.labels = labels

    def __getitem__(self, index):
        data, target = self.datas[index], self.labels[index] 
        return data, target

    def __len__(self):
        return len(self.datas)


train_dataloader = DataLoader(MyDataset(training_data, training_label), batch_size=batch_size,drop_last=True)

#print('train_dataloader',len(train_dataloader))

class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size) 
        self.fc2 = nn.Linear(hidden_size, hidden_size)
        self.fc3 = nn.Linear(hidden_size, num_classes)
    
    def forward(self, x):
        out = F.relu(self.fc1(x))
        out = self.fc2(out)
        out = self.fc3(out)
        return F.log_softmax(out, dim=1)
    
net = Net(input_size, hidden_size, num_classes)

    
criterion = nn.NLLLoss()
# criterion =  nn.BCEWithLogitsLoss()
optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate)


training_losses = []
# Train the Model
for epoch in range(num_epochs):
    batch_losses = []
    for data, labels in train_dataloader:
        # print(data.shape)  
        # Convert torch tensor to Variable
        data = Variable(data)
        labels = Variable(labels)
        
        # Forward + Backward + Optimize
        optimizer.zero_grad()  # zero the gradient buffer
        outputs = net(data)
        loss = criterion(outputs, labels.long())
        batch_losses.append(loss.data.cpu().numpy())
        loss.backward()
        optimizer.step()
        #if epoch % 5 == 0:
        #    print ('Epoch [%d/%d], Loss: %.4f' %(epoch+1, num_epochs, loss.item()))

    training_loss = np.mean(np.array(batch_losses))
    training_losses.append(training_loss)
    if epoch % 20 == 0:
        print(f"[{epoch+1}] Training loss: {training_loss:.3f}\t")



net.cpu()
net = net.eval()  # NOTE: important!

with open("0mlresult.txt", "w+") as f:
    with torch.no_grad():

        # for batch_id, (x,y) in enumerate(train_dataloader):
        #     # print(batch_id,len(x),y)
        #     print(x[0])
        #     datain = Variable(x)
        #     output = net(datain)
        #     prediction = int(torch.max(output.data, 1)[1].numpy())
        #     print(y,prediction)

        # print(sentence_embeddings[1].shape)


        for i in range(10):

            tstdata = sentence_embeddings[i].reshape(1,768)
            # print(tstdata.shape)
            datain = Variable(torch.from_numpy(tstdata))
            predicted = net(datain).data.numpy()
            print(y[i],predicted)

            # prediction = int(np.max(predicted))
            # print(y[i],prediction)
        # print(datain)

        # datain = Variable(torch.from_numpy(training_data[1]))
        # predicted = net(datain)
        # print(predicted)
        

        # for data, labels in train_dataloader:
        #     datain = Variable(data)
        #     predicted = net(datain)
        #     print(predicted.shape)
        #     for element in predicted:
        #         f.write(str(element)+'\n')



        # for i in range(0, 100):
        #     # print(x_tn[i],y_tn[i])
        #     # xin = Variable(torch.from_numpy(x[i]))
        #     print(len(training_data[i]))
        #     print(torch.from_numpy(training_data[i]).shape)
        #     xin = Variable(torch.from_numpy(training_data[i]))
        #     #xin = torch.FloatTensor(torch.from_numpy(training_data[i]))
        #     predicted = net(xin)
 
        #     for element in predicted:
        #         # print(element)
        #         f.write(str(y_tn[i])+'\t'+str(element.item())+'\n')

        # f.write('\nREAL TEST\n')
        # for i in range(700, 800):
        #     xin = Variable(torch.from_numpy(x[i]))
        #     predicted = model(xin).data.numpy()
        #     for element in predicted:
        #         # print(element)
        #         f.write(str(y_tn[i])+'\t'+str(element.item())+'\n')       