import os

os.system('pip install -U wandb')

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
# wandb.init(project="transformers")


# os.system('/opt/conda/bin/python -m pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ../input/apexpytorch') 

os.system('pip install --upgrade transformers')
os.system('pip install simpletransformers')

import logging


logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)

import pandas as pd

# import shutil
# shutil.copytree('/kaggle/input/fastbertdata/sample_data/imdb_movie_reviews','/kaggle/working/data')
datapath ='/kaggle/input/fastbertdata/sample_data/imdb_movie_reviews/data/'


train_df = pd.read_csv(datapath +'train.csv', error_bad_lines=False)
eval_df = pd.read_csv(datapath+'val.csv', error_bad_lines=False)

# train_df = pd.read_csv('data/data/train5000.csv', error_bad_lines=False)
# val_df = pd.read_csv('data/data/val500.csv', error_bad_lines=False)

train_df = train_df.rename({'label': 'labels'}).dropna()
eval_df = eval_df.rename({'label': 'labels'}).dropna()
train_df.drop('index',axis=1, inplace=True)
eval_df.drop('index',axis=1, inplace=True)

# print(train_df.head())

train_args={
    # 'reprocess_input_data': True,
    # 'overwrite_output_dir': True,
    'fp16': False,
    'num_train_epochs': 50,
    'save_model_every_epoch': False,
    'save_eval_checkpoints': False,
    'save_steps':2000000000000000000,
    'wandb_project': 'transformers',
    'overwrite_output_dir': True,
    'train_batch_size': 48, #8
    'evaluate_during_training':True,
}

from simpletransformers.classification import ClassificationModel

# model = ClassificationModel('roberta', 'roberta-base',args=train_args)
model = ClassificationModel('roberta', '/kaggle/input/roberta-base',args=train_args)

model.train_model(train_df,eval_df=eval_df)

result, model_outputs, wrong_predictions = model.eval_model(eval_df)

print(result, model_outputs, wrong_predictions)

print("positive pred")
predictions, raw_outputs = model.predict(['This movie is so good! I first seen it when i was six','This is one of the best movies I have ever seen...','The 100 Best Movies of the Decade'])

print(predictions, raw_outputs)

print("Negative pred")
predictions, raw_outputs = model.predict(['very bad','absolutely not intresting','Game unplayable, absolutely terrible performance','worst movies of all time'])

print(predictions, raw_outputs)







