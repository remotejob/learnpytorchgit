import os

os.system('pip install -U wandb')

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
# wandb.init(project="transformers")


# os.system('/opt/conda/bin/python -m pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ../input/apexpytorch') 

os.system('pip install --upgrade transformers')
os.system('pip install simpletransformers')

import logging
import torch


logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)

import pandas as pd
import numpy as np

# import shutil
# shutil.copytree('/kaggle/input/fastbertdata/sample_data/imdb_movie_reviews','/kaggle/working/data')
datapath ='/kaggle/input/simpletransbotdata/'
# datapath ='/kaggle/input/agnewsdata/'



# train_df = pd.read_csv(datapath+'train.csv', header=None)
# train_df['text'] = train_df.iloc[:, 1] + " " + train_df.iloc[:, 2]
# train_df = train_df.drop(train_df.columns[[1, 2]], axis=1)
# train_df.columns = ['labels', 'text']
# train_df = train_df[['text', 'labels']]
# train_df['text'] = train_df['text'].apply(lambda x: x.replace('\\', ' '))
# train_df['labels'] = train_df['labels'].apply(lambda x:x-1)

# eval_df = pd.read_csv(datapath+'test.csv', header=None)
# eval_df['text'] = eval_df.iloc[:, 1] + " " + eval_df.iloc[:, 2]
# eval_df = eval_df.drop(eval_df.columns[[1, 2]], axis=1)
# eval_df.columns = ['labels', 'text']
# eval_df = eval_df[['text', 'labels']]
# eval_df['text'] = eval_df['text'].apply(lambda x: x.replace('\\', ' '))
# eval_df['labels'] = eval_df['labels'].apply(lambda x:x-1)
# print(train_df.dtypes)
# print(eval_df.dtypes) 
# print(train_df.labels.nunique())
# nlabels = train_df.labels.nunique()
# print(train_df.head())



train_df = pd.read_csv(datapath +'train.csv', error_bad_lines=False,skiprows=1,header=None)
eval_df = pd.read_csv(datapath+'val.csv', error_bad_lines=False,skiprows=1,header=None)
train_df.dropna(inplace = True) 
eval_df.dropna(inplace = True) 

# print(train_df.dtypes)
# print(eval_df.dtypes)

train_df.columns = ['text', 'labels']
eval_df.columns = ['text', 'labels']

print(train_df.dtypes)
print(eval_df.dtypes)


train_df['labels'] = train_df['labels']
eval_df['labels'] = eval_df['labels']


# train_df.columns = ["text","labels"]
# eval_df.columns = ["text","labels"]

# train_df = train_df.rename({'label': 'labels'}).dropna()
# eval_df = eval_df.rename({'label': 'labels'}).dropna()

# print(train_df.dtypes)
# count_df = train_df.groupby('labels')
print('--------------------------')
print(train_df.labels.nunique())

num_labels = train_df.labels.nunique()

#print(train_df.head())
print('--------------------------')

# print(train_df.shape)
# print(eval_df.shape)
# train_df = train_df.astype({'labels':np.int32})
# eval_df = eval_df.astype({'labels':np.int32})
# train_df[['text']] = train_df[['text']].astype('str')
# eval_df[['text']] = eval_df[['text']].astype('str')

# train_df.text = train_df.text.astype(str,copy=True, errors='raise')
# eval_df.text = eval_df.text.astype(str,copy=True, errors='raise')

# print(train_df.dtypes)
# print(eval_df.dtypes) 


# train_df.dropna(inplace = True) 
# eval_df.dropna(inplace = True) 

# print(train_df.labels.nunique())

# train_df = pd.read_csv('data/data/train5000.csv', error_bad_lines=False)
# val_df = pd.read_csv('data/data/val500.csv', error_bad_lines=False)

# train_df = train_df.rename({'label': 'labels'}).dropna()
# eval_df = eval_df.rename({'label': 'labels'}).dropna()
# train_df.drop('index',axis=1, inplace=True)
# eval_df.drop('index',axis=1, inplace=True)

#print(train_df.head())

train_args={
    'fp16': False,
    'num_train_epochs': 350,
    'best_model_dir': 'best_model/',
    'save_model_every_epoch': False,
    'save_eval_checkpoints': False,
    'save_steps':2000,
    'wandb_project': 'transformers',
    'overwrite_output_dir': True,
    'train_batch_size': 124, #8 #48 #
    'eval_batch_size': 96,
    'reprocess_input_data': True,
    'do_lower_case': True,
    'logging_steps': 100,
    'evaluate_during_training':True
}

from simpletransformers.classification import ClassificationModel

# model = ClassificationModel('bert', 'bert-base-multilingual-uncased',use_cuda=False,args=train_args)
# model = ClassificationModel('roberta', 'roberta-base',use_cuda=True,args=train_args,num_labels=65)
#model = ClassificationModel('bert','bert-base-finnish-cased-v1',use_cuda=True,args=train_args,num_labels=num_labels)
model = ClassificationModel('bert','/kaggle/input/bertfinnishdata',use_cuda=True,args=train_args,num_labels=num_labels)
# model = ClassificationModel('roberta', '/kaggle/input/roberta-base',args=train_args)

model.train_model(train_df,eval_df=eval_df)

result, model_outputs, wrong_predictions = model.eval_model(eval_df)

print(result, model_outputs, wrong_predictions)

print('asia on selvä soitan','i_call','soitan kulli seisoo varaa minulle pillua','i_call_if','mä asun turussa','i_live_at')

print("positive pred")
predictions, raw_outputs = model.predict(['asia on selvä soitan','soitan kulli seisoo varaa minulle pillua','mä asun turussa','bed classification'])

print(predictions, raw_outputs)

for raw in raw_outputs:
    print(type(raw))
    print(raw.max().item())
    print(raw.mean().item())
    print(np.argmax(raw))
    
    top1_prob,top1label = torch.topk(torch.from_numpy(raw),1)
    print('---------------------------')
    print(top1_prob,top1label)

# print("Negative pred")
# predictions, raw_outputs = model.predict(['very bad','absolutely not intresting','Game unplayable, absolutely terrible performance','worst movies of all time'])

# print(predictions, raw_outputs)







