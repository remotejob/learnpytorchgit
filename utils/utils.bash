for i in $(seq 1 10000) ; do
    awk -v seed=$RANDOM 'BEGIN{srand(seed);print int(rand()*2)}'
done

paste -d"\t" data/ensentences.tsv data/y.txt > data/sentences.tsv 


sed 's/<eos>/\'$'\n/g' output/generated.txt

sed -i 's/ <eos> /\'$'\n/g' output/generated.txt

sed -i 's/ <eos> /. /g' output/generated.txt
sed -i 's/\.\./. /g' output/generated.txt
sed -i 's/  / /g' output/generated.txt


cat data/filterdtw/filteredtw.csv | awk 'NR%10!=1' > data/filterdtw/train.txt
cat data/filterdtw/filteredtw.csv | awk 'NR%10==1' > data/filterdtw/valid.txt

sed -i ':a;N;$!ba;s/\n/<\/s>\'$'\n/g' data/filterdtw/train.txt
sed -i ':a;N;$!ba;s/\n/<\/s>\'$'\n/g' data/filterdtw/valid.txt
sed -i ':a;N;$!ba;s/\n/<\/s>\'$'\n/g' data/filterdtw/test.txt

spm_train --input=data/filterdtw/train.txt,data/filterdtw/valid.txt --model_prefix=data/m --vocab_size=10000 --num_threads=32 --input_sentence_size=100000 --shuffle_input_sentence=true

spm_encode  --output_format=id --model=data/m.model < data/filterdtw/test.txt > data/filterdtw/testbpe.txt
spm_encode  --output_format=id --model=data/m.model < data/filterdtw/train.txt > data/filterdtw/trainbpe.txt
spm_encode  --output_format=id --model=data/m.model < data/filterdtw/valid.txt > data/filterdtw/validbpe.txt

spm_decode --model=data/m.model --input_format=id  < output/generated.txt > output/result.txt

14.04.2020
scp 192.168.1.202:/tmp/asktbl.tsv data/bot/
cat data/bot/asktbl.tsv | awk 'NR%10!=1' > data/bot/train.tsv
cat data/bot/asktbl.tsv | awk 'NR%10==1' > data/bot/val.tsv
cp data/bot/val.tsv data/bot/test.tsv

cat data/toxic/train.csv | awk 'NR%10!=1' > data/toxic/tmp.csv
mv data/toxic/tmp.csv data/toxic/train.csv
cat data/toxic/train.csv | awk 'NR%10==1' > data/toxic/valid.csv

cat data/toxicsmall/train_preprocessed.csv| awk 'NR%10!=1' > data/toxicsmall/tmp.csv
mv data/toxicsmall/tmp.csv data/toxicsmall/train_preprocessed.csv
cat data/toxicsmall/train_preprocessed.csv| awk 'NR%10==1' > data/toxicsmall/valid_preprocessed.csv


awk  'BEGIN {FS=",";OFS="\","} {$1=$1} {print $2,$3}' data/toxicsmall/train_preprocessed.csv >tmp
sed -i 's/^/\"/' tmp
mv tmp data/toxicsmall/train_preprocessed.csv
awk  'BEGIN {FS=",";OFS="\","} {$1=$1} {print $2,$3}' data/toxicsmall/valid_preprocessed.csv >tmp
sed -i 's/^/\"/' tmp
mv tmp data/toxicsmall/valid_preprocessed.csv

17.04.2020
cat data/quora.org.csv | awk 'NR%10==1' > data/quoravalid.csv
cat data/quora.org.csv | awk 'NR%10!=1' > data/tmp
mv data/tmp data/quoratrain.csv

20.04.2020
cat databot/asktbl.tsv | awk 'NR%10!=1' > tmp
cat databot/asktbl.tsv | awk 'NR%10==1' > databot/test.tsv
mv tmp databot/train.tsv


cut -f 2 fastbertdata/imdb/train.csv

awk  'BEGIN {FS=",";OFS="\","} {$1=$1} {print $2,$3}' fastbertdata/imdb/train.csv


cat fastbertdata/sample_data/imdb_movie_reviews/data/train.csv.org | awk 'NR%10!=1' > fastbertdata/sample_data/imdb_movie_reviews/data/train.csv
cat fastbertdata/sample_data/imdb_movie_reviews/data/train.csv | awk 'NR%10==1' > fastbertdata/sample_data/imdb_movie_reviews/data/val.csv

cat simpletransdata/train.csv.org | awk 'NR%10!=1' > simpletransdata/train.csv
cp simpletransdata/train.csv.org simpletransdata/train.csv
cat simpletransdata/train.csv.org | awk 'NR%10==1' > simpletransdata/val.csv


30.04.2020
python utils/converter.py --tf_checkpoint_path bertfinnishdataorg/bert-base-finnish-cased  --bert_config_file bertfinnishdataorg/bert-base-finnish-cased/bert_config.json --pytorch_dump_path bertfinnishdata







