conda activate kaggle

rm -rf output/*
export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
kaggle kernels output almazseo/learnpytorch -p output/

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
kaggle kernels output kagkagdevprod/learnpytorch -p output/


spm_decode --model=data/m.model --input_format=id  < output/generated.txt > output/result.txt

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
kaggle kernels output almazseo/womenreview -p output/
cp output/saved_weights.pt data/models

#simpletransbot3
export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
kaggle kernels output almazurov/simpletransbot3 -p output/


#bertwandbsimple
export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
kaggle kernels output almazurov/bertwandbsimple -p output/
