# https://gist.github.com/santi-pdp/d0e9002afe74db04aa5bbff6d076e8fe
import os
os.system('pip install -U wandb')
os.system('pip install -U sentence-transformers')
from sentence_transformers import SentenceTransformer

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb

wandb.init(project="transformers")

import torch
import torch.optim as optim
from torch.autograd import Variable
import torch.nn as nn
import numpy as np
import torch.nn.functional as F


SEED = 2019

device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Torch
torch.manual_seed(SEED)


numrec = 8000

x_tn, y_tn = np.genfromtxt('../input/learnpytorchdata/sentences.tsv',
                           delimiter='\t', usecols=(1, 2), dtype=str, comments=None, unpack=True)

modeltr = SentenceTransformer('bert-base-nli-mean-tokens')

sentences = []

for i in range(0, numrec):
    sentences.append(x_tn[i])

sentence_embeddings = modeltr.encode(sentences)


# X1 = torch.randn(1000, 768)
# print(X1)
# X2 = torch.randn(1000, 768) + 1.5
# print(X2)
#X2 = torch.randn(1000, 50)
# X = torch.cat([X1, X2], dim=0)
# X = sentence_embeddings
#X = torch.from_numpy(sentence_embeddings)
X = torch.FloatTensor(sentence_embeddings).to(device)
Y1 = torch.zeros(4000, 1)
Y2 = torch.ones(4000, 1)
Y = torch.cat([Y1, Y2], dim=0).to(device)
print(X.shape)
print(Y.shape)


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(768, 768)
        self.relu1 = nn.ReLU()
        self.dout = nn.Dropout(0.2)
        self.fc2 = nn.Linear(768, 100)
        # self.fc2 = nn.Linear(768, 768)
        self.prelu = nn.PReLU(1)
        self.out = nn.Linear(100, 1)
        # self.out = nn.Linear(768, 1)
        self.out_act = nn.Sigmoid()

    def forward(self, input_):
        a1 = F.relu(self.fc1(input_))
        h1 = self.relu1(a1)
        dout = self.dout(h1)
        a2 = self.fc2(dout)
        h2 = self.prelu(a2)
        a3 = self.out(h2)
        y = self.out_act(a3)
        return y


net = Net().to(device)
# opt = optim.Adam(net.parameters(), lr=0.001, betas=(0.9, 0.999))
criterion = nn.BCELoss()
# criterion=nn.CrossEntropyLoss()
opt=optim.Adam(net.parameters(),lr=0.001)

# def train_epoch(model, opt, criterion, batch_size=50):
def train_epoch(model, opt, criterion, batch_size=250):

    model.train()
    losses = []
    for beg_i in range(0, X.size(0), batch_size):
        x_batch = X[beg_i:beg_i + batch_size, :]
        y_batch = Y[beg_i:beg_i + batch_size, :]
        x_batch = Variable(x_batch)
        y_batch = Variable(y_batch)

        opt.zero_grad()
        # (1) Forward
        y_hat = model(x_batch)
        # (2) Compute diff
        loss = criterion(y_hat, y_batch)

        # wandb.log({"Loss": loss})
        # (3) Compute gradients
        # loss.backward()
        # (4) update weights
        opt.step()
        losses.append(loss.data.cpu().numpy())
    return losses


# e_losses = []
num_epochs = 30000

for e in range(num_epochs):
    e_losses = train_epoch(net, opt, criterion)
    wandb.log({"Loss": np.mean(e_losses)})

# print(e_losses)

# x_t = Variable(torch.randn(1, 768))
# net.eval()
# print(round(net(x_t).item()))
# x_1_t = Variable(torch.randn(1, 768) + 1.5)
# print(round(net(x_1_t).item()))

with open("0mlresult.txt", "w+") as f:
    with torch.no_grad():

        net.cpu()

        for i in range(200, 300):
            # print(x_tn[i],y_tn[i])
            xin = Variable(torch.from_numpy(sentence_embeddings[i]))
            prval = net(xin)
            print(round(prval.item()),prval.item())
            #predicted = net(xin)
 
            # for element in predicted:
            #     # print(element)
            #     f.write(str(Y[i])+'\t'+str(element.item())+'\n')
        print('\nREAL TEST\n')
        f.write('\nREAL TEST\n')
        for i in range(4200, 4230):
            xin = Variable(torch.from_numpy(sentence_embeddings[i]))
            prval = net(xin)
            print(round(prval.item()),prval.item())
            #predicted = net(xin)
            # predicted = net(xin).data.numpy()
            # for element in predicted:
            #     # print(element)
            #     f.write(str(Y[i])+'\t'+str(element.item())+'\n')
