# https://towardsdatascience.com/multiclass-text-classification-using-lstm-in-pytorch-eac56baed8df

import os
os.system('pip install -U wandb')
os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
wandb.init(project="transformers")

import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import re
import spacy
from collections import Counter
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
import string
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from sklearn.metrics import mean_squared_error

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

SEED = 2020

# Torch
torch.manual_seed(SEED)

# Cuda algorithms
torch.backends.cudnn.deterministic = True

# reviews = pd.read_csv("../input/womens-ecommerce-clothing-reviews/Womens Clothing E-Commerce Reviews.csv" )
reviews = pd.read_csv("../input/simpletransbotdata/train.csv.org" )
print(reviews.shape)
print(reviews.head())

# from sklearn.preprocessing import MultiLabelBinarizer

# mlb = MultiLabelBinarizer()


# reviews['Title'] = reviews['Title'].fillna('')
# reviews['Review Text'] = reviews['Review Text'].fillna('')
# reviews['review'] = reviews['Title'] + ' ' + reviews['Review Text']

# reviews = reviews[['review', 'Rating']]
reviews.columns = ['review', 'rating']
reviews['review_length'] = reviews['review'].apply(lambda x: len(x.split()))
print(reviews.head())

# zero_numbering = {1:0,2:1,3:2,4:3,5:4}
# reviews['rating'] = reviews['rating'].apply(lambda x: zero_numbering[x])
reviews['rating'] = reviews['rating'].apply(lambda x: x-1)


print(np.mean(reviews['review_length']))

tok = spacy.load('en')
def tokenize (text):
    text = re.sub(r"[^\x00-\x7F]+", " ", text)
    regex = re.compile('[' + re.escape(string.punctuation) + '0-9\\r\\t\\n]') # remove punctuation and numbers
    nopunct = regex.sub(" ", text.lower())
    return [token.text for token in tok.tokenizer(nopunct)]

counts = Counter()
for index, row in reviews.iterrows():
    counts.update(tokenize(row['review']))      


print("num_words before:",len(counts.keys()))
for word in list(counts):
    if counts[word] < 1:
        del counts[word]
print("num_words after:",len(counts.keys()))


vocab2index = {"":0, "UNK":1}
words = ["", "UNK"]
for word in counts:
    vocab2index[word] = len(words)
    words.append(word)

def encode_sentence(text, vocab2index, N=20):
    tokenized = tokenize(text)
    encoded = np.zeros(N, dtype=int)
    enc1 = np.array([vocab2index.get(word, vocab2index["UNK"]) for word in tokenized])
    length = min(N, len(enc1))
    encoded[:length] = enc1[:length]
    return encoded, length

reviews['encoded'] = reviews['review'].apply(lambda x: np.array(encode_sentence(x,vocab2index )))
# print(reviews.head())


print(Counter(reviews['rating']))

X = list(reviews['encoded'])
y = list(reviews['rating'])
from sklearn.model_selection import train_test_split
# X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.1,random_state=42)
X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.2)


# num_labels = X_train.rating.nunique()
# print("num_labels",num_labels)

class ReviewsDataset(Dataset):
    def __init__(self, X, Y):
        self.X = X
        self.y = Y
        
    def __len__(self):
        return len(self.y)
    
    def __getitem__(self, idx):
        return torch.from_numpy(self.X[idx][0].astype(np.int32)), self.y[idx], self.X[idx][1]

train_ds = ReviewsDataset(X_train, y_train)
valid_ds = ReviewsDataset(X_valid, y_valid)


def train_model(model, epochs=1000, lr=0.001):
    best_valid_loss = 0.0    
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = torch.optim.Adam(parameters, lr=lr)
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer,gamma=0.95)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer,30,gamma=0.95)

    for i in range(epochs):
        model.train()
        sum_loss = 0.0
        total = 0
        for x, y, l in train_dl:
            x = x.long()
            x.to(device)
            y = y.long()
            y.to(device)
            l = l.to(device)
            y_pred = model(x, l)
            optimizer.zero_grad()
            loss = F.cross_entropy(y_pred.to(device), y.to(device))
            loss.backward()
            optimizer.step()
            sum_loss += loss.item()*y.shape[0]
            total += y.shape[0]
        val_loss, val_acc, val_rmse = validation_metrics(model, val_dl)
  
        if i % 5 == 1:
            # print(type(val_acc),type(best_valid_loss))
            scalar_val_acc = val_acc.cpu().item()
            # print(scalar_val_acc,best_valid_loss)
            if  scalar_val_acc > best_valid_loss:
                print("Save best model!!!",scalar_val_acc,best_valid_loss,"epoch",i)
                best_valid_loss = val_acc.cpu().item()
                torch.save(model.state_dict(), 'saved_weights.pt')
            else:
                scheduler.step()
                wandb.log({"lr":scheduler.get_lr()[0]})   


            # if i > 500:
            #     scheduler.step()
            #     wandb.log({"lr":scheduler.get_lr()[0]}

            wandb.log({"epoch":i,"train_loss": sum_loss/total,"val_loss":val_loss,"val_acc":val_acc,"val_rmse":val_rmse})
            # print("train loss %.3f, val loss %.3f, val accuracy %.3f, and val rmse %.3f" % (sum_loss/total, val_loss, val_acc, val_rmse))

          

def validation_metrics (model, valid_dl):
    model.eval()
    correct = 0
    total = 0
    sum_loss = 0.0
    sum_rmse = 0.0
    for x, y, l in valid_dl:
        x, y, l = x.to(device), y.to(device), l.to(device)
        x = x.long()
        y = y.long()
        y_hat = model(x, l)
        loss = F.cross_entropy(y_hat, y)
        pred = torch.max(y_hat, 1)[1]
        correct += (pred == y).float().sum()
        total += y.shape[0]
        sum_loss += loss.item()*y.shape[0]
        sum_rmse += np.sqrt(mean_squared_error(pred.cpu(), y.cpu().unsqueeze(-1)))*y.cpu().shape[0]
    return sum_loss/total, correct/total, sum_rmse/total

batch_size = 50 #5000 10000 20000
vocab_size = len(words)
train_dl = DataLoader(train_ds, batch_size=batch_size, shuffle=True)
val_dl = DataLoader(valid_ds, batch_size=batch_size)

class LSTM_fixed_len(torch.nn.Module) :
    def __init__(self, vocab_size, embedding_dim, hidden_dim) :
        super().__init__()
        self.embeddings = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
        self.linear = nn.Linear(hidden_dim, 65)
        self.dropout = nn.Dropout(0.2)
        
    def forward(self, x, l):
        x = self.embeddings(x.to(device))
        x = self.dropout(x)
        lstm_out, (ht, ct) = self.lstm(x)
        return self.linear(ht[-1])

model_fixed =  LSTM_fixed_len(vocab_size, 50, 50)
model_fixed.to(device)
wandb.watch(model_fixed)

train_model(model_fixed, epochs=30500, lr=0.01)


def predict(model,text,expect):
    # model.eval()

    print(text+'--------'+expect)
    encodedtext,l  = encode_sentence(text,vocab2index)
    x = torch.from_numpy(encodedtext.astype(np.int32))

    # print("xshape2-->",x.shape)
    # print(x)
    x = x.unsqueeze(0)
    # print("xshape3-->",x.shape)
    output = model_fixed(x.long(), l)

    print('softmax------------')
    softmaxlabel = nn.functional.softmax(output)
    print(softmaxlabel)
    print('top2------------')
    top2_prob,top2label = torch.topk(output,2)
    print(top2_prob,top2label)
    # print('torch.max------------')
    # val,index = torch.max(softmaxlabel,0)
    # print(val,index)
    print('END------------')



path = 'saved_weights.pt'
model_fixed.load_state_dict(torch.load(path))
model_fixed.eval()

print("---------STARt TEST--------------")
predict(model_fixed,"botit säätänä","3") #3
predict(model_fixed,"en soita vittun lutka","14") #14
predict(model_fixed,"helsingissä asun","38") # 38
predict(model_fixed,"huomenta mitä mies","0") #??
predict(model_fixed,"iso blondi ihana","26")
predict(model_fixed,"joo minäpä soitan","43")
predict(model_fixed,"kalliita numeroita","15")
predict(model_fixed,"kerro mulle mitä haluat","35")
predict(model_fixed,"kone vastailee ei kiinnosta","3")
predict(model_fixed,"kuin vanha oot","16")






