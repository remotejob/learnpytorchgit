import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import re
import spacy
import string
from collections import Counter
# from torch.utils.data import Dataset, DataLoader
# import torch.nn.functional as F


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

reviews = pd.read_csv("simpletransdata/train.csv.org")
print(reviews.shape)
print(reviews.head())

reviews.columns = ['review', 'rating']
reviews['review_length'] = reviews['review'].apply(lambda x: len(x.split()))
reviews['rating'] = reviews['rating'].apply(lambda x: x-1)
print(reviews.head())

tok = spacy.load('en')
def tokenize (text):
    text = re.sub(r"[^\x00-\x7F]+", " ", text)
    regex = re.compile('[' + re.escape(string.punctuation) + '0-9\\r\\t\\n]') # remove punctuation and numbers
    nopunct = regex.sub(" ", text.lower())
    return [token.text for token in tok.tokenizer(nopunct)]

counts = Counter()
for index, row in reviews.iterrows():
    counts.update(tokenize(row['review']))

# print(counts)   


print(np.mean(reviews['review_length']))

print("num_words before:",len(counts.keys()))
for word in list(counts):
    if counts[word] < 1:
        del counts[word]
print("num_words after:",len(counts.keys()))


vocab2index = {"":0, "UNK":1}
words = ["", "UNK"]
for word in counts:
    vocab2index[word] = len(words)
    words.append(word)

# print(words)

def encode_sentence(text, vocab2index, N=20):
    tokenized = tokenize(text)
    encoded = np.zeros(N, dtype=int)
    enc1 = np.array([vocab2index.get(word, vocab2index["UNK"]) for word in tokenized])
    length = min(N, len(enc1))
    encoded[:length] = enc1[:length]
    return encoded, length

reviews['encoded'] = reviews['review'].apply(lambda x: np.array(encode_sentence(x,vocab2index )))

print(reviews.head())

print(Counter(reviews['rating']))


class LSTM_fixed_len(torch.nn.Module) :
    def __init__(self, vocab_size, embedding_dim, hidden_dim) :
        super().__init__()
        self.embeddings = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
        self.linear = nn.Linear(hidden_dim, 65)
        self.dropout = nn.Dropout(0.2)
        
    def forward(self, x, l):
        x = self.embeddings(x.to(device))
        x = self.dropout(x)
        lstm_out, (ht, ct) = self.lstm(x)
        return self.linear(ht[-1])

vocab_size = len(words)


model_fixed =  LSTM_fixed_len(vocab_size, 50, 50)


def predict(model,text,expect):
    # model.eval()

    print(text+'--------'+expect)
    encodedtext,l  = encode_sentence(text,vocab2index)
    x = torch.from_numpy(encodedtext.astype(np.int32))

    # print("xshape2-->",x.shape)
    # print(x)
    x = x.unsqueeze(0)
    # print("xshape3-->",x.shape)
    output = model_fixed(x.long(), l)

    print('softmax------------')
    softmaxlabel = nn.functional.softmax(output)
    print(softmaxlabel)
    print('top2------------')
    top2_prob,top2label = torch.topk(output,1)
    print(top2_prob,top2label)

    print(softmaxlabel[0][top2label[0]])
    # print('torch.max------------')
    # val,index = torch.max(softmaxlabel,0)
    # print(val,index)
    print('END------------')



path = 'data/models/saved_weights.pt'
model_fixed.load_state_dict(torch.load(path,map_location=torch.device('cpu')))
model_fixed.eval()

print("---------STARt TEST--------------")
predict(model_fixed,"botit säätänä","3") #3
predict(model_fixed,"en soita vittun lutka","14") #14
predict(model_fixed,"helsingissä asun","38") # 38
predict(model_fixed,"huomenta mitä mies","0") #??
predict(model_fixed,"iso blondi ihana","26")
predict(model_fixed,"joo minäpä soitan","43")
predict(model_fixed,"kalliita numeroita","15")
predict(model_fixed,"kerro mulle mitä haluat","35")
predict(model_fixed,"kone vastailee ei kiinnosta","3")
predict(model_fixed,"kuin vanha oot","16")

print("real test")
predict(model_fixed,"haluttiin kovasti","i_like_it")
predict(model_fixed,"olen tosi ujo","you_are_beautiful")

predict(model_fixed,"haluatko raskaaksi","do_you_want")

