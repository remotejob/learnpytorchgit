# https://medium.com/@sangramsing/language-modeling-in-torchtext-a9810954ab0c
# https://github.com/pytorch/examples/tree/master/word_language_model

import sys

import argparse
import time
import math
import os
import torch
import torch.nn as nn
import torch.onnx

os.system('pip install -U wandb')

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
wandb.init(project="transformers")


sys.path.insert(0, "/kaggle/input/learnpytorchdata/src")

import data
import model

torch.manual_seed(48)

device = "cuda"

corpus = data.Corpus('/kaggle/input/learnpytorchdata/filterdtw')
# corpus = data.Corpus('/kaggle/input/learnpytorchdata/wikitext-2-raw')


def batchify(data, bsz):
    # Work out how cleanly we can divide the dataset into bsz parts.
    nbatch = data.size(0) // bsz
    # Trim off any extra elements that wouldn't cleanly fit (remainders).
    data = data.narrow(0, 0, nbatch * bsz)
    # Evenly divide the data across the bsz batches.
    data = data.view(bsz, -1).t().contiguous()
    return data.to(device)

eval_batch_size = 10
train_data = batchify(corpus.train, 20)
val_data = batchify(corpus.valid, eval_batch_size)
test_data = batchify(corpus.test, eval_batch_size)

ntokens = len(corpus.dictionary)

print(ntokens)

model = model.RNNModel('LSTM', ntokens, 200, 200, 2, 0.2, 'store_true').to(device)

wandb.watch(model)

criterion = nn.NLLLoss()


def repackage_hidden(h):
    """Wraps hidden states in new Tensors, to detach them from their history."""

    if isinstance(h, torch.Tensor):
        return h.detach()
    else:
        return tuple(repackage_hidden(v) for v in h)

def get_batch(source, i):
    seq_len = min(35, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].view(-1)
    return data, target


def evaluate(data_source):
    # Turn on evaluation mode which disables dropout.
    model.eval()
    total_loss = 0.
    ntokens = len(corpus.dictionary)
    # if args.model != 'Transformer':
    hidden = model.init_hidden(eval_batch_size)
    with torch.no_grad():
        for i in range(0, data_source.size(0) - 1, 35):
            data, targets = get_batch(data_source, i)
            # if args.model == 'Transformer':
            #     output = model(data)
            #     output = output.view(-1, ntokens)
            # else:
            output, hidden = model(data, hidden)
            hidden = repackage_hidden(hidden)
            total_loss += len(data) * criterion(output, targets).item()
    return total_loss / (len(data_source) - 1)    



def train():
    # Turn on training mode which enables dropout.
    model.train()
    total_loss = 0.
    start_time = time.time()
    ntokens = len(corpus.dictionary)
    # if args.model != 'Transformer':
    hidden = model.init_hidden(20)
    for batch, i in enumerate(range(0, train_data.size(0) - 1, 35)):
        data, targets = get_batch(train_data, i)
        # Starting each batch, we detach the hidden state from how it was previously produced.
        # If we didn't, the model would try backpropagating all the way to start of the dataset.
        model.zero_grad()
        # if args.model == 'Transformer':
        #     output = model(data)
        #     output = output.view(-1, ntokens)
        # else:
        hidden = repackage_hidden(hidden)
        output, hidden = model(data, hidden)
        loss = criterion(output, targets)
        wandb.log({"train_loss": loss})
        loss.backward()

        # `clip_grad_norm` helps prevent the exploding gradient problem in RNNs / LSTMs.
        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.25)
        for p in model.parameters():
            p.data.add_(-lr, p.grad.data)

        total_loss += loss.item()

        if batch % 200 == 0 and batch > 0:
            cur_loss = total_loss / 200
            elapsed = time.time() - start_time
            print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.5f} | ms/batch {:5.2f} | '
                    'loss {:5.2f} | ppl {:8.2f}'.format(
                epoch, batch, len(train_data) // 35, 20,
                elapsed * 1000 / 200, cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()


lr = 20
best_val_loss = None


for epoch in range(1, 35+1):
    epoch_start_time = time.time()
    train()
    val_loss = evaluate(val_data)
    wandb.log({"val_loss": val_loss})
    print('-' * 89)
    print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
            'valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time),
                                        val_loss, math.exp(val_loss)))
    print('-' * 89)
    # Save the model if the validation loss is the best we've seen so far.
    if not best_val_loss or val_loss < best_val_loss:
        with open('model.pt', 'wb') as f:
            torch.save(model, f)
        best_val_loss = val_loss
    else:
        # Anneal the learning rate if no improvement has been seen in the validation dataset.
        lr /= 4.0



model.eval()

hidden = model.init_hidden(1)
input = torch.randint(ntokens, (1, 1), dtype=torch.long).to(device)

with open('generated.txt', 'w') as outf:
    with torch.no_grad():  # no tracking history
        for i in range(1000):
            # if is_transformer_model:
            #     output = model(input, False)
            #     word_weights = output[-1].squeeze().div(args.temperature).exp().cpu()
            #     word_idx = torch.multinomial(word_weights, 1)[0]
            #     word_tensor = torch.Tensor([[word_idx]]).long().to(device)
            #     input = torch.cat([input, word_tensor], 0)
            # else:
            output, hidden = model(input, hidden)
            word_weights = output.squeeze().div(1).exp().cpu()
            word_idx = torch.multinomial(word_weights, 1)[0]
            input.fill_(word_idx)

            word = corpus.dictionary.idx2word[word_idx]

            outf.write(word + ('\n' if i % 20 == 19 else ' '))

            if i % 100 == 0:
                print('| Generated {}/{} words'.format(i, 1000))