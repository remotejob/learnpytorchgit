# https://stackoverflow.com/questions/48675563/simple-pytorch-example-loss-on-training-doesnt-decrease


from __future__ import division

import os
os.system('pip install -U wandb')
os.system('pip install -U sentence-transformers')
from sentence_transformers import SentenceTransformer

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb

wandb.init(project="transformers")
import numpy as np
# import matplotlib.pyplot as plt

import torch
import torch.utils.data as utils_data
from torch.autograd import Variable
from torch import optim, nn
from torch.utils.data import Dataset 
import torch.nn.functional as F

# from sklearn.datasets import load_boston


cuda=True


#regular old numpy
# boston = load_boston()

# x=boston.data
# y=boston.target

# print(x.shape)
# print(y.shape)
# print(x[0])
# print(y[0])


numrec = 500

x_tn, y_tn = np.genfromtxt('../input/learnpytorchdata/sentences.tsv',
                           delimiter='\t', usecols=(1, 2), dtype=str, comments=None, unpack=True)

modeltr = SentenceTransformer('bert-base-nli-mean-tokens')

sentences = []

for i in range(0, numrec):
    sentences.append(x_tn[i])

sentence_embeddings = modeltr.encode(sentences)

X = torch.FloatTensor(sentence_embeddings)

# Y1 = torch.zeros(4000, 1)
# Y2 = torch.ones(4000, 1)
# Y = torch.cat([Y1, Y2], dim=0)
Y = []
for i in range(0, numrec):
    Y.append(y_tn[i].astype(np.float)) 

Y = np.asarray(Y,dtype=np.float32)

# Y = yar.astype(np.float)
print(X.shape)
# print(Y.shape)

# training_samples = utils_data.TensorDataset(torch.from_numpy(X), torch.from_numpy(Y))
training_samples = utils_data.TensorDataset(X, torch.from_numpy(Y))
data_loader = utils_data.DataLoader(training_samples, batch_size=1)

print(len(data_loader)) #number of batches in an epoch

#override this
class Net(nn.Module):
    def __init__(self):
         super(Net, self).__init__()

         #all the layers
         self.fc1   = nn.Linear(X.shape[1], 40)
         self.drop = nn.Dropout(p=0.2)
         self.fc2   = nn.Linear(40, 1)

    #    
    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.drop(x)
        x = self.fc2(x)

        return x



net=Net()
if cuda:
    net.cuda()
print(net)

# create a stochastic gradient descent optimizer
optimizer = optim.Adam(net.parameters(),lr=0.001)
# optim_scheduler = optim.lr_scheduler.StepLR(optimizer, 10, 0.989)
optim_scheduler = optim.lr_scheduler.StepLR(optimizer, 10, 0.1)
# create a loss function (mse)
loss = nn.MSELoss()
# loss = nn.BCELoss()

# # create a stochastic gradient descent optimizer
# optimizer = optim.Adam(net.parameters())
# # create a loss function (mse)
# loss = nn.MSELoss()

# run the main training loop
epochs =500
# hold_loss=[]

for epoch in range(epochs):
    # cum_loss=0.
    for batch_idx, (data, target) in enumerate(data_loader):
        tr_x, tr_y = Variable(data.float()), Variable(target.float())
        if cuda:
            tr_x, tr_y = tr_x.cuda(), tr_y.cuda() 

        # Reset gradient
        optimizer.zero_grad()

        # Forward pass
        fx = net(tr_x)
        output = loss(fx, tr_y) #loss for this batch
        wandb.log({"Loss": output})


        # cum_loss += output.data[0] 

        # Backward 
        output.backward()

        # Update parameters based on backprop

       
        optimizer.step()
        optim_scheduler.step()
    # hold_loss.append(cum_loss)
    if epoch % 5 == 0:
        print('Epoch-{0} lr: {1}'.format(epoch, optimizer.param_groups[0]['lr']))   


with open("0mlresult.txt", "w+") as f:
    with torch.no_grad():

        net.cpu()

        for i in range(200, 300):
            # print(x_tn[i],y_tn[i])
            xin = Variable(torch.from_numpy(sentence_embeddings[i]))
            prval = net(xin)
            print(Y[i],round(prval.item()),prval.item())
            #predicted = net(xin)
 
            # for element in predicted:
            #     # print(element)
            #     f.write(str(Y[i])+'\t'+str(element.item())+'\n')
        # print('\nREAL TEST\n')
        # f.write('\nREAL TEST\n')
        # for i in range(4200, 4230):
        #     xin = Variable(torch.from_numpy(sentence_embeddings[i]))
        #     prval = net(xin)
        #     print(Y[i],round(prval.item()),prval.item())
            #predicted = net(xin)
            # predicted = net(xin).data.numpy()
            # for element in predicted:
            #     # print(element)
            #     f.write(str(Y[i])+'\t'+str(element.item())+'\n')
