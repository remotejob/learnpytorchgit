
# This Python 3 environment comes with many helpful analytics libraries installed
import os
os.system('pip install -U sentence-transformers')

from sentence_transformers import SentenceTransformer
from torch.autograd import Variable
import torch.optim as optim
import torch.nn as nn
import torch
import random
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import numpy as np  # linear algebra
from torch.utils.data import DataLoader, Dataset


input_size = 768 #6
hidden_size = 768 #6
num_classes = 2
num_epochs = 1000
batch_size = 10
learning_rate = 0.01
numrec = 1000

device = 'cuda' if torch.cuda.is_available() else 'cpu'

x_tn, y_tn = np.genfromtxt('../input/learnpytorchdata/sentences.tsv',
                           delimiter='\t', usecols=(1, 2), dtype=str, comments=None, unpack=True)

y = []
# for i in range(0, numrec):
#     y.append(int(y_tn[i]))

# training_label = torch.LongTensor(y)                         

y_train = torch.from_numpy(y_tn.astype(np.float))
for i in range(0, numrec):
    y.append(y_train[i])

# print(y_train.shape)
# y_train.resize_(10000, 1)
# y_tn = y_train

modeltr = SentenceTransformer('bert-base-nli-mean-tokens')

sentences = []

for i in range(numrec):
    sentences.append(x_tn[i])

sentence_embeddings = modeltr.encode(sentences)


class MyDataset(Dataset):
    def __init__(self, datas, labels):
        self.datas = datas
        self.labels = labels

    def __getitem__(self, index):
        data, target = self.datas[index], self.labels[index] 
        return data, target

    def __len__(self):
        return len(self.datas)


x = sentence_embeddings
# y = training_label

train_dataloader = DataLoader(MyDataset(x, y), batch_size=batch_size,drop_last=True)


model = nn.Linear(768, 1)  # predict logits for 5 classes
#model = nn.LSTM(768, 1)
# model


criterion = nn.BCEWithLogitsLoss()
optimizer = optim.SGD(model.parameters(), lr=1e-2)

training_losses = []

for epoch in range(num_epochs):
    batch_losses = []
    for data, labels in train_dataloader:
        optimizer.zero_grad()
        data = Variable(data)
        labels = Variable(labels)
        output = model(data)
        loss = criterion(output, labels)
        batch_losses.append(loss.data.cpu().numpy())
        optimizer.step()

    # for i in range(len(x)):
    #     optimizer.zero_grad()
    #     xin = Variable(torch.from_numpy(x[i]).to(device))
    #     output = model(xin)

    #     loss = criterion(output, y[i].to(device))
    #     batch_losses.append(loss.data.cpu().numpy())
    #     loss.backward()
    #     optimizer.step()

    training_loss = np.mean(np.array(batch_losses))
    training_losses.append(training_loss)


    if epoch % 5 == 0:
        #print('Loss: {:.3f}'.format(loss.item()))
        print(f"[{epoch+1}] Training loss: {training_loss:.3f}\t")


model.cpu()
model = model.eval()  # NOTE: important!

with open("0mlresult.txt", "w+") as f:
    with torch.no_grad():

        for i in range(0, 50):
            # print(x_tn[i],y_tn[i])
            xin = Variable(torch.from_numpy(x[i]))
            predicted = model(xin).data.numpy()
 
            for element in predicted:
                # print(element)
                f.write(str(y_tn[i])+'\t'+str(element.item())+'\n')

        f.write('\nREAL TEST\n')
        for i in range(0, 50):
            xin = Variable(torch.from_numpy(x[i]))
            predicted = model(xin).data.numpy()
            for element in predicted:
                # print(element)
                f.write(str(y_tn[i])+'\t'+str(element.item())+'\n')
