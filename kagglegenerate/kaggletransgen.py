###############################################################################
# Language Modeling on Wikitext-2
#
# This file generates new sentences sampled from the language model
#
###############################################################################

# import argparse
import sys

sys.path.insert(0, "/kaggle/input/learnpytorchdata/src")

import data

import torch

import torch.nn.functional as F
import torch.nn as nn
import math


# import data

# parser = argparse.ArgumentParser(description='PyTorch Wikitext-2 Language Model')

# # Model parameters.
# parser.add_argument('--data', type=str, default='./data/wikitext-2',
#                     help='location of the data corpus')
# parser.add_argument('--checkpoint', type=str, default='./model.pt',
#                     help='model checkpoint to use')
# parser.add_argument('--outf', type=str, default='generated.txt',
#                     help='output file for generated text')
# parser.add_argument('--words', type=int, default='1000',
#                     help='number of words to generate')
# parser.add_argument('--seed', type=int, default=1111,
#                     help='random seed')
# parser.add_argument('--cuda', action='store_true',
#                     help='use CUDA')
# parser.add_argument('--temperature', type=float, default=1.0,
#                     help='temperature - higher will increase diversity')
# parser.add_argument('--log-interval', type=int, default=100,
#                     help='reporting interval')
# args = parser.parse_args()

# Set the random seed manually for reproducibility.
torch.manual_seed(48)
# if torch.cuda.is_available():
#     if not args.cuda:
#         print("WARNING: You have a CUDA device, so you should probably run with --cuda")

# device = torch.device("cuda" if args.cuda else "cpu")
device = 'cuda'

class PositionalEncoding(nn.Module):
    r"""Inject some information about the relative or absolute position of the tokens
        in the sequence. The positional encodings have the same dimension as
        the embeddings, so that the two can be summed. Here, we use sine and cosine
        functions of different frequencies.
    .. math::
        \text{PosEncoder}(pos, 2i) = sin(pos/10000^(2i/d_model))
        \text{PosEncoder}(pos, 2i+1) = cos(pos/10000^(2i/d_model))
        \text{where pos is the word position and i is the embed idx)
    Args:
        d_model: the embed dim (required).
        dropout: the dropout value (default=0.1).
        max_len: the max. length of the incoming sequence (default=5000).
    Examples:
        >>> pos_encoder = PositionalEncoding(d_model)
    """

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(
            0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        r"""Inputs of forward function
        Args:
            x: the sequence fed to the positional encoder model (required).
        Shape:
            x: [sequence length, batch size, embed dim]
            output: [sequence length, batch size, embed dim]
        Examples:
            >>> output = pos_encoder(x)
        """

        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)


class TransformerModel(nn.Module):
    """Container module with an encoder, a recurrent or transformer module, and a decoder."""

    def __init__(self, ntoken, ninp, nhead, nhid, nlayers, dropout=0.5):
        super(TransformerModel, self).__init__()
        # try:
        from torch.nn import TransformerEncoder, TransformerEncoderLayer
        # except:
        #     raise ImportError('TransformerEncoder module does not exist in PyTorch 1.1 or lower.')
        self.model_type = 'Transformer'
        self.src_mask = None
        self.pos_encoder = PositionalEncoding(ninp, dropout)
        encoder_layers = TransformerEncoderLayer(ninp, nhead, nhid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.encoder = nn.Embedding(ntoken, ninp)
        self.ninp = ninp
        self.decoder = nn.Linear(ninp, ntoken)

        self.init_weights()

    def _generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float(
            '-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    def init_weights(self):
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, src, has_mask=True):
        if has_mask:
            device = src.device
            if self.src_mask is None or self.src_mask.size(0) != len(src):
                mask = self._generate_square_subsequent_mask(
                    len(src)).to(device)
                self.src_mask = mask
        else:
            self.src_mask = None

        src = self.encoder(src) * math.sqrt(self.ninp)
        src = self.pos_encoder(src)
        output = self.transformer_encoder(src, self.src_mask)
        output = self.decoder(output)
        return F.log_softmax(output, dim=-1)

# if args.temperature < 1e-3:
#     parser.error("--temperature has to be greater or equal 1e-3")

with open('/kaggle/input/learnpytorchdata/models/model.pt', 'rb') as f:
    model = torch.load(f).to(device)
model.eval()

# corpus = data.Corpus('/kaggle/input/learnpytorchdata/filterdtw')
# # corpus = data.Corpus('/kaggle/input/learnpytorchdata/wikitext-2-raw')
# ntokens = len(corpus.dictionary)

# iword = corpus.dictionary.word2idx['porno']
# print('iword',iword)

# word= corpus.dictionary.idx2word[iword]
# print('word',word)


# input = torch.randint(ntokens, (1, 1), dtype=torch.long).to(device)

# is_transformer_model = hasattr(model, 'model_type') and model.model_type == 'Transformer'
# if not is_transformer_model:
# hidden = model.init_hidden(1)
# input0 = torch.randint(ntokens, (1, 1), dtype=torch.long).to(device)
# print(input0.shape)
input = torch.tensor([[5000]],dtype=torch.long).to(device)
# print(input.shape)

with open('generated.txt', 'w') as outf:
    with torch.no_grad():  # no tracking history
        for i in range(1000):
            # if is_transformer_model:
            output = model(input, False)
            word_weights = output[-1].squeeze().div(1.0).exp().cpu()
            word_idx = torch.multinomial(word_weights, 1)[0]
            word_tensor = torch.Tensor([[word_idx]]).long().to(device)
            input = word_tensor 
            # input = torch.cat([input, word_tensor], 0)
            # print(input)
            # else:
            # output, hidden = model(input, hidden)
            # word_weights = output.squeeze().div(1.0).exp().cpu()
            # word_idx = torch.multinomial(word_weights, 1)[0]
            # input.fill_(word_idx)

            # word = corpus.dictionary.idx2word[word_idx]


            word =word_idx.item()

            outf.write(str(word) + ('\n' if i % 20 == 19 else ' '))

            # if i % 100 == 0:
            #     print('| Generated {}/{} words'.format(i, 1000))